import React from 'react';
import './MoneyBook.css';


const MoneyBook = props => {
  let totalCost = 0;
  if (props.costs.length === 0) {
  return <div className="container">У Вас пока нет записей</div>
  }
  return (
    <div className="container">
      {props.costs.map((cost, id) =>{
        totalCost+= cost.cost;
        return <div className="moneyBook" key={id}>
          <p className={cost.type}></p>
          <span>
            <span>{cost.itemName}</span>
            <span>{cost.cost} COM</span>
          </span>
          <button onClick={()=> props.remuveCost(id)} className='remuve'>X</button></div>
      })}
      <p className="total">Итого потрачено: {totalCost} com</p>
    </div>
  )
}

export default MoneyBook;
