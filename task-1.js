const tasks = [
  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

 const getTotalTimeinFrontEnd = () => {
   let time = 0;
   tasks.map((task) => {
     if (task.category === 'Frontend'){
       time +=task.timeSpent;
     }
   })
   return time;
 };

 const getTotalTimeinBug = () => {
   let time = 0;
   tasks.map((task) => {
     if (task.type === 'bug'){
       time +=task.timeSpent;
     }
   })
   return time;
 };

let tasksWithUi = tasks.filter((element) => element.title.indexOf('UI') != -1);

const getTasksType = () => {
  const tasksType = {Frontend: 0, Backend: 0};
  tasks.map((task) =>{
    switch (task.category) {
      case 'Frontend':
        tasksType.Frontend ++;
        break;
      case 'Backend':
        tasksType.Backend ++;
        break;
      default: return null;
    }
  })
  return tasksType;
};

let tasksWithMoreTime = tasks.filter((element) => element.timeSpent >= 4).map((element)=>{return {title: element.title, category: element.category}});


console.log(getTotalTimeinFrontEnd());
console.log(getTotalTimeinBug());
console.log(tasksWithUi);
console.log(getTasksType());
console.log(tasksWithMoreTime);
