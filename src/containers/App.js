import React, { Component } from 'react';
import './App.css';
import AddMoneyForm from '../components/AddMoneyForm/AddMoneyForm';
import MoneyBook from '../components/MoneyBook/MoneyBook';
import Stata from '../components/Stata/Stata';

class App extends Component {
  state = {
    costs: []
  }
  clearValues = () => {
    document.getElementById('itemName').value = null;
    document.getElementById('cost').value = null;
  }

  addNewCost = (e) =>{
    e.preventDefault();
    let costs = [...this.state.costs];
    const type = document.getElementById('type').value;
    const itemName = document.getElementById('itemName').value;
    const cost = parseInt((document.getElementById('cost').value), 10);
    const newCost = {type, itemName, cost};
    costs.push(newCost);
    this.setState({costs})
    this.clearValues();
  };

  remuveCost = (index) =>{
    let costs = [...this.state.costs];
    costs.splice(index, 1);
    this.setState({costs})
  };


  render() {
    return (
      <div className="App">
        <AddMoneyForm addCost={this.addNewCost} />
        <Stata costs={this.state.costs} />
        <MoneyBook
          costs={this.state.costs}
          remuveCost={this.remuveCost}/>
      </div>
    );
  }
}

export default App;
