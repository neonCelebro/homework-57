import React from 'react';
import './AddMoneyForm.css';

const categories = ['Entertaiment','Food','Car'];

const AddMoneyForm = props => {
  return (
    <div>
      <form onSubmit={props.addCost}>
        <select id='type'>
          {categories.map((category,index) =>{
            return <option key={index} value={category}>{category}</option>
          })}
        </select>
        <span className='cost'><input maxLength='30' required id='itemName' placeholder='Затраты' type='text'/></span>
        <span className='cost'><input required id='cost' placeholder='стоимость' type='number'/></span>
        <span className='cost'>KGS</span>
        <input type='submit' id='send' value ='Отправить'/>

      </form>
    </div>
  )
}

export default AddMoneyForm;
