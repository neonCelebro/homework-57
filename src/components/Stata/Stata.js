import React from 'react';
import './Stata.css';


const Stata = (props) => {
  let totalEntertaiment = 0;
  let totalFood = 0;
  let totalCar = 0;
  let totalCost = 0;
  props.costs.map((cost) =>{
    switch (cost.type) {
      case 'Entertaiment':
      return totalEntertaiment += cost.cost;
      case 'Food':
      return totalFood += cost.cost;
      case 'Car':
      return totalCar += cost.cost;
      default: return null;
    }
  });
    totalCost += totalEntertaiment + totalFood + totalCar;
  const styleEntrtaiment = () => {
    if (totalCost !== 0) {
      return {width: (((totalEntertaiment / totalCost) * 100) * 5) + 'px'}
    } else {
      return {width: '0px'}
    }
  };
  const styleFood = () => {
    if (totalCost !== 0) {
      return {width: (((totalFood / totalCost) * 100) * 5) + 'px'}
    } else {
      return {width: '0px'}
    }
  };
  const styleCar = () => {
    if (totalCost !== 0) {
      return {width: (((totalCar / totalCost) * 100) * 5) + 'px'}
    } else {
      return {width: '0px'}
    }
  };
    return (
    <div className="Stata">
      <span style={styleEntrtaiment()} className='stataEntrtaiment'></span>
      <span style={styleFood()} className='stataFood'></span>
      <span style={styleCar()} className='stataCar'></span>
      <div className='legend'>
        <div className='legendEntertaiment'><span className='Entertaiment'></span></div>
        <div className='legendFood'><span className='Food'></span></div>
        <div className='legendCar'><span className='Car'></span></div>
      </div>
    </div>
  )
}

export default Stata;
